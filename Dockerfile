FROM python:3.7

WORKDIR /app

COPY . /app

RUN pip install --upgrade pip

RUN unlink /etc/localtime

RUN ln -s /usr/share/zoneinfo/Etc/GMT-3 /etc/localtime


RUN pip install boto3 pandas psycopg2

