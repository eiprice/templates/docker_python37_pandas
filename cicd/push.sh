#!/bin/sh

echo "Login to Gitlab Docker"
docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com 

echo "Preparation task"
echo "Build Docker Image"
docker build -t $REGISTRY_REPOSITORY .

echo "Push to ECR Repository"
docker tag $REGISTRY_REPOSITORY:latest $REGISTRY_REPOSITORY:$CI_COMMIT_SHA
docker push $REGISTRY_REPOSITORY:latest
docker push $REGISTRY_REPOSITORY:$CI_COMMIT_SHA
